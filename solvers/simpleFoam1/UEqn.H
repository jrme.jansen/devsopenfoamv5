    // Momentum predictor

    MRF.correctBoundaryVelocity(U);

    tmp<fvVectorMatrix> tUEqn
    (
        fvm::div(phi, U)
      + MRF.DDt(U)
      + turbulence->divDevReff(U)
     ==
        fvOptions(U)
    );

    fvVectorMatrix& UEqn = tUEqn.ref();

    // under-relax the U equation
    UEqn.relax();

    fvOptions.constrain(UEqn);

    // contribution of the pressure equation is not added to the
    // source therm of the matrix object UEqn
    if (simple.momentumPredictor())
    {
        solve(UEqn == -fvc::grad(p));

        fvOptions.correct(U);
    }
