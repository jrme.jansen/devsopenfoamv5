/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2016-2017 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "LuoKuang.H"
#include "addToRunTimeSelectionTable.H"
#include "surfaceFields.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
namespace viscosityModels
{
    defineTypeNameAndDebug(LuoKuang, 0);
    addToRunTimeSelectionTable
    (
        viscosityModel,
        LuoKuang,
        dictionary
    );
}
}


// * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * * //
/*inspiration de casson avec la technique des min max */
Foam::tmp<Foam::volScalarField>
Foam::viscosityModels::LuoKuang::calcNu() const
{
    return max
    (
        nuMin_,
        min
        (
            nuMax_, n1_+n2_/( sqrt(max(strainRate(),dimensionedScalar("VSMALL", dimless/dimTime, VSMALL)))  )+tau0_ / ( max(strainRate(),dimensionedScalar("VSMALL", dimless/dimTime, VSMALL)) )
        )
    );
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::viscosityModels::LuoKuang::LuoKuang
(
    const word& name,
    const dictionary& viscosityProperties,
    const volVectorField& U,
    const surfaceScalarField& phi
)
:
    viscosityModel(name, viscosityProperties, U, phi),
    LuoKuangCoeffs_(viscosityProperties.optionalSubDict(typeName + "Coeffs")),
    n1_("n1", dimViscosity, LuoKuangCoeffs_),
    n2_("n2", dimViscosity*pow(dimTime,-0.5), LuoKuangCoeffs_),  //dimViscosity m2s^-1  sqrt(dimeTime) s^(1/2)
    tau0_("tau0", dimViscosity/dimTime, LuoKuangCoeffs_),
    nuMin_("nuMin", dimViscosity, LuoKuangCoeffs_),
    nuMax_("nuMax", dimViscosity, LuoKuangCoeffs_),
    nu_
    (
        IOobject
        (
            "nu",
            U_.time().timeName(),
            U_.db(),
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        calcNu()
    )
{}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

bool Foam::viscosityModels::LuoKuang::read
(
    const dictionary& viscosityProperties
)
{
    viscosityModel::read(viscosityProperties);

    LuoKuangCoeffs_ = viscosityProperties.optionalSubDict(typeName + "Coeffs");

    LuoKuangCoeffs_.lookup("n1") >> n1_;
    LuoKuangCoeffs_.lookup("n2") >> n2_;
    LuoKuangCoeffs_.lookup("tau0") >> tau0_;
    LuoKuangCoeffs_.lookup("nuMin_") >> nuMin_;
    LuoKuangCoeffs_.lookup("nuMax_") >> nuMax_;

    return true;
}


// ************************************************************************* //
